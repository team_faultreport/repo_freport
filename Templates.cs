﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FaultyReportingForm
{
    using Sitecore.Data;
    public struct Templates
    {
        public struct FaultReport
        {
            public static readonly ID firstName = new ID("{EE0AB3F8-07D8-4827-B589-9D0644A1BD7D}");
            public static readonly ID lastName = new ID("{388DBD6E-B0A8-430C-AC82-9975FE7BA218}");
            public static readonly ID email = new ID("{BDEB53A3-A7B7-4A6C-BADF-278D1222A0EB}");
            public static readonly ID phone = new ID("{FEC70259-60F3-48BC-B799-A594DCC7760D}");
            public static readonly ID faultType = new ID("{44DC6813-2E0C-48AE-9B42-2EDFFAB87DFA}");
            public static readonly ID departureStation = new ID("{76F0FDEB-9413-422B-B810-B79EFBDB7E63}");
            public static readonly ID departureTime = new ID("{A9A9820D-8FE4-49C1-ACB8-598DE3604310}");
            public static readonly ID destinationStation = new ID("{943AD3B2-1838-4BCF-B6D3-B72050ED6A89}");
            public static readonly ID faultlocation = new ID("{7D7DF05E-548C-4B65-B85A-7AC83B79B6AE}");
            public static readonly ID faultTime = new ID("{DB8D0921-C42B-4204-A63A-DADE99770125}");
            public static readonly ID faultDescription = new ID("{67B73D6E-8A67-4D55-B3C3-89070A021029}");
            public static readonly ID Photo = new ID("{D48B2D39-ED37-4350-ACCE-6B0552A42C66}");
            //public static readonly ID departureStation_SF = new ID("{7E0C3FF7-969B-4C97-89D0-B6FA990B6E7B}");
            //public static readonly ID faultlocation_SF = new ID("{C77EDDE7-4603-43C7-ABE0-47919FAA3BC6}");
            //public static readonly ID faultDescription_SF = new ID("{B2E29443-8E3B-4CE9-9C42-99149712B8B4}");
            //public static readonly ID Photo_SF = new ID("{31F76AA1-BEC1-4EFE-8398-0060238A1F0F}");
        }
        public struct StationInformation
        {
            public static readonly ID StationName = new ID("{2FA507D0-A3B0-4135-8071-F830EBC1ECEC}");
            public const string StationName_FieldName = "StationName";
            public static readonly ID CRSCode = new ID("{27A8853E-CAFB-4729-AE16-417E84658F34}");
            public const string CRSCode_FieldName = "CRSCode";
            public static readonly ID StationAddress = new ID("{76717F32-715B-4F22-9394-87E3ED57DFC7}");
            public const string StationAddress_FieldName = "StationAddress";
            public static readonly ID XPox = new ID("{1D9109B2-D166-4D8C-B20D-F48600CB9A48}");
            public const string XPox_FieldName = "XPox";
            public static readonly ID YPos = new ID("{E524D823-F4FE-498B-86D8-DF2E16D40F7F}");
            public const string YPos_FieldName = "YPos";

            //Station Validity
            public static readonly ID StationStatus = new ID("{F12170E6-50DE-4CD0-B823-91BF98C822F5}");
            public const string StationStatus_FieldName = "StationStatus";
            public static readonly ID ValidFrom = new ID("{6A3E5FF2-5ACC-4829-9BF7-FBE18B6A5417}");
            public const string ValidFrom_FieldName = "ValidFrom";
            public static readonly ID ValidTo = new ID("{D85D3A9E-1599-4F84-9E32-444CDE18D429}");
            public const string ValidTo_FieldName = "ValidTo";

        }
        public struct StationStatus
        {
            public static readonly ID StationStatusIcon = new ID("{F1A524B2-A298-4BEA-9337-F35CC71CF838}");
            public const string StationStatusIcon_FieldName = "StationStatusIcon";
            public static readonly ID StationStatusTitle = new ID("{9EA6F999-14A7-49A6-A324-22DC8691BE77}");
            public const string StationStatusTitle_FieldName = "StationStatusTitle";
            public static readonly ID StationStatusDetails = new ID("{875EBE1B-E7E0-4B81-A031-2698CC826156}");
            public const string StationStatusDetails_FieldName = "StationStatusDetails";

        }

        public struct ConfigData
        {

            public static readonly ID host = new ID("{4C2B79DF-298A-4641-AEDD-DB56E431F262}");
            public static readonly ID port = new ID("{DB6F95CD-E449-413E-BF0E-6880BFD4891A}");
            public static readonly ID username = new ID("{1FB5F156-9EA3-4AC6-B174-948DC9D91AC7}");
            public static readonly ID password = new ID("{8161EC5B-7B01-4289-A19E-BC70DCA3E6ED}");
            public static readonly ID ssl = new ID("{F880B1D1-605C-45F6-8186-B5C4E7C3A297}");
            public static readonly ID fromAddress = new ID("{5FA3C139-C2AD-4F9B-B97D-A2815C0D12CD}");
            public static readonly ID toAddress = new ID("{C29BA2BE-9405-41AA-9165-F06E63EC3B76}");
            public static readonly ID senderdisplay = new ID("{B6FE0984-EFF8-4B3A-A312-5C2EE3D0DC6B}");
            public static readonly ID subject = new ID("{FDFE6134-F8E2-485C-9CB0-DAFB8E4AA090}");
            public static readonly ID subjectSWR = new ID("{C4147087-5AC1-493F-BA2C-341EE87DFC34}");
            public static readonly ID secretkey = new ID("{CB449E6C-15C5-43FB-B036-77FC3EEA43D3}");
            public static readonly ID sitekey = new ID("{38313142-BA45-4098-B078-52B774580B58}");
            public static readonly ID MailCC = new ID("{CB851183-F649-42EA-A355-77B16185AF27}");

        }
    }
}