﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Mail;
using System.IO;
using FaultyReportingForm.Models;

namespace FaultyReportingForm.Controllers
{
    public class FaultyFormController : Controller
    {
        
        //configuration var
        //private const int Timeout = 180000;
        private readonly string _host;
        private readonly int _port;
        private readonly string _user;
        private readonly string _pass;
        private readonly bool _ssl;
        public string SenderDisplay { get; set; }
        public MailAddress SenderMailAddress { get; set; }
        public MailAddress RecipientMailAddress { get; set; }
        public string Subject { get; set; }

        public string subjectSWR { get; set; }
        public string Body { get; set; }
        public string BodySWR { get; set; }
        public string BodySWRAtRail { get; set; }
        public string CC { get; set; }

        public FaultyFormController(MailAddress fromAddress, MailAddress toAddress, Sitecore.Data.Items.Item mailconfiguraionObj)
        {
            ////MailServer - Represents the SMTP Server
            //_host = ConfigurationManager.AppSettings["MailServer"];
            ////Port- Represents the port number
            //_port = int.Parse(ConfigurationManager.AppSettings["Port"]);
            ////MailAuthUser and MailAuthPass - Used for Authentication for sending email
            //_user = ConfigurationManager.AppSettings["MailAuthUser"];
            //_pass = ConfigurationManager.AppSettings["MailAuthPass"];
            //_ssl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSSL"]);
            //SenderMailAddress = fromAddress;
            //RecipientMailAddress = toAddress;
            //SenderDisplay = ConfigurationManager.AppSettings["SenderDisplay"];
            //Subject = ConfigurationManager.AppSettings["Subject"];
            //Body = ConfigurationManager.AppSettings["Body"];
            ////Body = "Hello There";

            //new Changes
            _host = mailconfiguraionObj.Fields[Templates.ConfigData.host].ToString();
            //Port- Represents the port number
            string _port1 = (mailconfiguraionObj.Fields[Templates.ConfigData.port]).ToString();
            _port = Convert.ToInt32(_port1);
            //MailAuthUser and MailAuthPass - Used for Authentication for sending email
            _user = mailconfiguraionObj.Fields[Templates.ConfigData.username].ToString();
            _pass = mailconfiguraionObj.Fields[Templates.ConfigData.password].ToString();
            string _sslstr = (mailconfiguraionObj.Fields[Templates.ConfigData.ssl]).ToString();
            _ssl = _sslstr == "true" ? true : false;
            SenderMailAddress = fromAddress;
            RecipientMailAddress = toAddress;
            CC = mailconfiguraionObj.Fields[Templates.ConfigData.MailCC].ToString();
            SenderDisplay = mailconfiguraionObj.Fields[Templates.ConfigData.senderdisplay].ToString();
            Subject = mailconfiguraionObj.Fields[Templates.ConfigData.subject].ToString();
            subjectSWR = mailconfiguraionObj.Fields[Templates.ConfigData.subjectSWR].ToString();
            Body = ConfigurationManager.AppSettings["Body"];

        }

        public void SendMail()
        {
            try
            {

                MailMessage eMail = new MailMessage(SenderMailAddress, RecipientMailAddress) { IsBodyHtml = true, Priority = MailPriority.Normal};
                eMail.Subject = Subject;             
                eMail.Body = Body;
                SmtpClient smtp = new SmtpClient(_host, _port);
                smtp.Credentials = new System.Net.NetworkCredential(_user, _pass);
                smtp.EnableSsl = _ssl;
                smtp.Send(eMail);
                eMail.Dispose();
                smtp.Dispose();

            }
            catch(Exception ex)
            {
               
                throw new Exception("Error Message : " + ex.Message);
            }
        }

        public void SendMailToSWR(FaultyForm_Mod model, HttpPostedFileBase file)
        {
            try
            {
                MailMessage eMail = new MailMessage(SenderMailAddress, RecipientMailAddress) { IsBodyHtml = true, Priority = MailPriority.Normal };
                //eMail.Subject = ConfigurationManager.AppSettings["SubjectSWR"];
                eMail.Subject = subjectSWR;
                if (CC != "")
                    eMail.CC.Add(CC);
                if (model.faultType == "On a SWR train")
                {
                    eMail.Body = ConfigurationManager.AppSettings["BodySWR"];
                    eMail.Body = model.faultType != null ? eMail.Body.Replace("faultType", "\'" + model.faultType + "\'") : eMail.Body.Replace("faultType", "");
                    eMail.Body = model.firstName != null ? eMail.Body.Replace("FirstName", model.firstName) : eMail.Body.Replace("FirstName", "");
                    eMail.Body = model.lastName != null ? eMail.Body.Replace("LastName", model.lastName) : eMail.Body.Replace("LastName", "");
                    eMail.Body = model.email != null ? eMail.Body.Replace("EmailID", model.email) : eMail.Body.Replace("EmailID", "");
                    eMail.Body = model.phone != null ? eMail.Body.Replace("PhoneNumber", model.phone) : eMail.Body.Replace("PhoneNumber", "");

                    if (model.faultType == "On a SWR train" || model.faultType != "At a rail station")
                    {
                        eMail.Body = model.destinationStation != null ? eMail.Body.Replace("DelDestinationStation", model.destinationStation) : eMail.Body.Replace("DelDestinationStation", "");
                        if (model.faultTime.ToString() == "1/1/0001 12:00:00 AM" || model.faultTime.ToString() == "01/01/0001 00: 00" || model.faultTime.ToString() == "01/01/0001 00:00:00" || model.faultTime < DateTime.Now.AddMonths(-6))
                        {
                            eMail.Body = eMail.Body.Replace("DelFaultTime", DateTime.Now.ToString("dd'/'MM'/'yyyy HH: mm"));
                        }
                        else
                        {
                            eMail.Body = eMail.Body.Replace("DelFaultTime", model.faultTime.ToString("dd'/'MM'/'yyyy HH: mm"));
                        }
                        //eMail.Body = model.faultTime.ToString() != null ? eMail.Body.Replace("DelFaultTime", model.faultTime.ToString()) : eMail.Body.Replace("DelFaultTime", "");
                        eMail.Body = model.departureTime != null ? eMail.Body.Replace("DelDepartureTime", model.departureTime) : eMail.Body.Replace("DelDepartureTime", "");
                        eMail.Body = model.faultlocation != null ? eMail.Body.Replace("DelFaultLocation", model.faultlocation) : eMail.Body.Replace("DelFaultLocation", "");
                        eMail.Body = model.departureStation != null ? eMail.Body.Replace("DelDepartureStation", model.departureStation) : eMail.Body.Replace("DelDepartureStation", "");
                        eMail.Body = model.faultDescription != null ? eMail.Body.Replace("DelFaultDescription", model.faultDescription) : eMail.Body.Replace("DelFaultDescription", "");
                    }
                    //else if (model.faultType == "At a rail station" || model.faultType != "On a SWR train")
                    //{
                    //    eMail.Body = eMail.Body.Replace("DelDestinationStation", "");
                    //    eMail.Body = eMail.Body.Replace("DelFaultTime", "");
                    //    eMail.Body = eMail.Body.Replace("DelDepartureTime", "");
                    //    eMail.Body = model.faultLocationDropdown != null ? eMail.Body.Replace("DelFaultLocation", model.faultLocationDropdown) : eMail.Body.Replace("DelFaultLocation", "");
                    //    eMail.Body = model.StnDepartureStation != null ? eMail.Body.Replace("DelDepartureStation", model.StnDepartureStation) : eMail.Body.Replace("DelDepartureStation", "");
                    //    eMail.Body = model.stnFaultDescription != null ? eMail.Body.Replace("DelFaultDescription", model.stnFaultDescription) : eMail.Body.Replace("DelFaultDescription", "");

                    //}
                }
                else if (model.faultType == "At a rail station")
                {
                   
                    eMail.Body = ConfigurationManager.AppSettings["BodySWRAtRail"];
                    eMail.Body = model.faultType != null ? eMail.Body.Replace("faultType", "\'" + model.faultType + "\'") : eMail.Body.Replace("faultType", "");
                    eMail.Body = model.firstName != null ? eMail.Body.Replace("FirstName", model.firstName) : eMail.Body.Replace("FirstName", "");
                    eMail.Body = model.lastName != null ? eMail.Body.Replace("LastName", model.lastName) : eMail.Body.Replace("LastName", "");
                    eMail.Body = model.email != null ? eMail.Body.Replace("EmailID", model.email) : eMail.Body.Replace("EmailID", "");
                    eMail.Body = model.phone != null ? eMail.Body.Replace("PhoneNumber", model.phone) : eMail.Body.Replace("PhoneNumber", "");

                    if (model.faultLocationDropdown == "Other")
                    {
                        eMail.Body = model.Other != null ? eMail.Body.Replace("DelFaultLocation", model.Other) : eMail.Body.Replace("DelFaultLocation", "");
                    }
                    else
                    {
                        eMail.Body = model.faultLocationDropdown != "Please select one" ? eMail.Body.Replace("DelFaultLocation", model.faultLocationDropdown) : eMail.Body.Replace("DelFaultLocation", "");
                    }

                    eMail.Body = model.StnDepartureStation != null ? eMail.Body.Replace("DelDepartureStation", model.StnDepartureStation) : eMail.Body.Replace("DelDepartureStation", "");
                    eMail.Body = model.stnFaultDescription != null ? eMail.Body.Replace("DelFaultDescription", model.stnFaultDescription) : eMail.Body.Replace("DelFaultDescription", "");
                }

                eMail.IsBodyHtml = true;


                if (file != null)
                {
                    string fileName = Path.GetFileName(file.FileName);
                    eMail.Attachments.Add(new Attachment(file.InputStream, fileName));
                }
                SmtpClient smtp = new SmtpClient(_host, _port);
                smtp.Credentials = new System.Net.NetworkCredential(_user, _pass);
                smtp.EnableSsl = _ssl;
                smtp.Send(eMail);
                eMail.Dispose();
                smtp.Dispose();

            }
            catch (Exception ex)
            {

                throw new Exception("Error Message : " + ex.Message);
            }
        }
    }
}