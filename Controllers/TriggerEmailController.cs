﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FaultyReportingForm.Models;
using System.Threading.Tasks;
using Sitecore.Data;
using System.Net;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Net.Mail;
using Sitecore.Configuration;
using Sitecore.Links;
using Sitecore.Data.Items;

namespace FaultyReportingForm.Controllers
{
    public class TriggerEmailController : Controller
    {
        // GET: TriggerEmail
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index_Post(FaultyForm_Mod model, HttpPostedFileBase file)
        {


            var response = Request["g-recaptcha-response"];
            //string secretKey = "6LeQ4EYUAAAAAE6FYlBya-DJJUXnzZyikaQFVgvM";
            string secretKey = ConfigurationManager.AppSettings["secretKey"];
            var client = new WebClient();
            var result = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, response));
            var obj = JObject.Parse(result);
            var status = (bool)obj.SelectToken("success");
            if (status)
            {
                //send auto respond
                    SendAutoRespond(model);
                //mail to SWR
                    SendMailtoSWR(model, file);
                //send to Sitecore
                SubmitValuesToSitecore(model, file);
                Database DB = Sitecore.Context.Database;
                Sitecore.Data.Items.Item redirect = DB.GetItem("/sitecore/content/Home/IndexPost");
                var url = LinkManager.GetItemUrl(redirect);
                return Redirect(url);
                //return View("");
            }
            else
            {
                //ViewBag.Message = "Google reCaptcha validation failed";
                //return View("Index");
                Database DB1 = Sitecore.Context.Database;
                Sitecore.Data.Items.Item redirect1 = DB1.GetItem("/sitecore/content/Home/Index");
                var url1 = LinkManager.GetItemUrl(redirect1);
                return Redirect(url1);
            }

        }

        
       

        private void SendAutoRespond(FaultyForm_Mod model)
        {
            //FaultyFormController triggerEmail = new FaultyFormController(new System.Net.Mail.MailAddress(ConfigurationManager.AppSettings["FromAddress"]), new System.Net.Mail.MailAddress(model.email));
            //triggerEmail.SendMail();

            Database DB = Sitecore.Data.Database.GetDatabase("master");
            Sitecore.Data.Items.Item mailconfiguraionObj = DB.GetItem("/sitecore/content/Home/Configuration/eMail");
            FaultyFormController triggerEmail = new FaultyFormController(new System.Net.Mail.MailAddress(mailconfiguraionObj.Fields[Templates.ConfigData.fromAddress].ToString()), new System.Net.Mail.MailAddress(model.email), mailconfiguraionObj);
            triggerEmail.SendMail();
        }

        private void SendMailtoSWR(FaultyForm_Mod model, HttpPostedFileBase file)
        {

            //FaultyFormController triggerEmail = new FaultyFormController(new System.Net.Mail.MailAddress(ConfigurationManager.AppSettings["FromAddress"]), new System.Net.Mail.MailAddress("omasood@q3tech.com; hmehra @q3tech.com"));
            //triggerEmail.SendMailToSWR(model, file);

             Database DB = Sitecore.Data.Database.GetDatabase("master");
            Sitecore.Data.Items.Item mailconfiguraionObj = DB.GetItem("/sitecore/content/Home/Configuration/eMail");
            string toAddress = mailconfiguraionObj.Fields[Templates.ConfigData.toAddress].ToString();
            FaultyFormController triggerEmail = new FaultyFormController(new System.Net.Mail.MailAddress(mailconfiguraionObj.Fields[Templates.ConfigData.fromAddress].ToString()), new System.Net.Mail.MailAddress(toAddress), mailconfiguraionObj);

            triggerEmail.SendMailToSWR(model, file);
        }

        private void SubmitValuesToSitecore(FaultyForm_Mod model, HttpPostedFileBase file)
        {
            Sitecore.Data.Items.Item newFault;
            string itemName= "";
            try
            {
                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    //Database DB = Sitecore.Context.Database;
                    Database DB = Sitecore.Data.Database.GetDatabase("master");
                    Sitecore.Data.Items.Item parentItem = DB.GetItem("/sitecore/content/home/List");
                    Sitecore.Data.Items.TemplateItem template = DB.GetItem("/sitecore/templates/FaultReportForm");
                    if (parentItem != null && template != null)
                    {
                        if(model.firstName != null)
                        {
                            string timeNow = DateTime.Now.ToString("dd'/'MM'/'yyyy HH: mm").Replace("/", "-").Replace(":", "-");
                            itemName = "Fault " + model.firstName + " " + timeNow;
                            newFault = parentItem.Add(itemName, template);
                        }
                        else
                            newFault = parentItem.Add("Fault Blank", template);

                        newFault.Editing.BeginEdit();
                        newFault.Fields[Templates.FaultReport.firstName].Value = model.firstName != null ? model.firstName :"";
                        newFault.Fields[Templates.FaultReport.lastName].Value = model.lastName != null ? model.lastName  : "";
                        newFault.Fields[Templates.FaultReport.email].Value = model.email != null ? model.email : "";
                        newFault.Fields[Templates.FaultReport.phone].Value = model.phone != null ? model.phone : "";
                        newFault.Fields[Templates.FaultReport.faultType].Value = model.faultType != "Please select one" ? model.faultType : "";

                        

                        if (model.faultType == "On a SWR train")
                        {
                            newFault.Fields[Templates.FaultReport.destinationStation].Value = model.destinationStation != null ? model.destinationStation : "";
                            if (model.faultTime.ToString() == "1/1/0001 12:00:00 AM" || model.faultTime.ToString() == "01/01/0001 00: 00" || model.faultTime.ToString() == "01/01/0001 00:00:00" || model.faultTime < DateTime.Now.AddMonths(-6))
                            {
                                newFault.Fields[Templates.FaultReport.faultTime].Value = DateTime.Now.ToString("dd'/'MM'/'yyyy HH: mm");
                            }
                            else
                            {
                                newFault.Fields[Templates.FaultReport.faultTime].Value = model.faultTime.ToString() != "" ? model.faultTime.ToString() : "";
                            }
                            newFault.Fields[Templates.FaultReport.departureTime].Value = model.departureTime != null ? model.departureTime : "";
                            newFault.Fields[Templates.FaultReport.faultlocation].Value = model.faultlocation != null ? model.faultlocation : "";
                            newFault.Fields[Templates.FaultReport.departureStation].Value = model.departureStation != null ? model.departureStation : "";
                            newFault.Fields[Templates.FaultReport.faultDescription].Value = model.faultDescription != null ? model.faultDescription : "";

                        }
                        else if(model.faultType == "At a rail station")
                        {
                            if (model.faultLocationDropdown == "Other")
                            {
                                newFault.Fields[Templates.FaultReport.faultlocation].Value = model.Other != null ? model.Other : "";
                            }
                            else
                            {
                                newFault.Fields[Templates.FaultReport.faultlocation].Value = model.faultLocationDropdown != null ? model.faultLocationDropdown : "";
                            }

                            newFault.Fields[Templates.FaultReport.destinationStation].Value = model.StnDepartureStation != null ? model.StnDepartureStation : "";
                            newFault.Fields[Templates.FaultReport.faultDescription].Value = model.stnFaultDescription != null ? model.stnFaultDescription : "";

                        }
                        newFault.Editing.EndEdit();

                        //upload image to sitecore
                        //UploadImage(file, itemName);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error Message : " + ex.Message);
                newFault.Editing.CancelEdit();
            }
        }

        private void UploadImage(HttpPostedFileBase file, string itemName)
        {
            Item List;
            try
            {
               
                string fileName = Path.GetFileName(file.FileName);
                string FileLocation = Path.Combine(Server.MapPath("~/img"), fileName);
                file.SaveAs(FileLocation);
                string itemPart = fileName.Length > 6 ? fileName.Substring(0, 5).Replace(".", "").Replace("x", "").Replace(" ", "") : fileName.Substring(0, fileName.Length).Replace(".", "").Replace("x", "").Replace(" ", "");
                /////
                //string mediaItemName = "/FaultGallery/" + itemPart;
                string mediaItemName = "/FaultGallery/LondonOver";
                Sitecore.Resources.Media.MediaCreatorOptions options = new Sitecore.Resources.Media.MediaCreatorOptions();
                options.FileBased = false;
                options.IncludeExtensionInItemName = false;
                options.OverwriteExisting = true;
                options.Versioned = false;
                options.Destination = "/sitecore/media library" + mediaItemName;
                options.Database = Sitecore.Configuration.Factory.GetDatabase("master");
                options.AlternateText = "LondonOver";
                Sitecore.Resources.Media.MediaCreator creator = new Sitecore.Resources.Media.MediaCreator();
                MediaItem mediaItem = creator.CreateFromFile(FileLocation, options);
                //return mediaItem;
                //using (new Sitecore.SecurityModel.SecurityDisabler())
                //{
                //    //Update Image to Fault Item
                //    Database DB = Factory.GetDatabase("master");
                //    //Item List = DB.GetItem("/sitecore/content/home/List/Fault daer 22-02-2018 19- 23");
                //    List = DB.GetItem("/sitecore/content/home/List/" + itemName);
                //    Item Photo = DB.GetItem("/sitecore/media library/" + mediaItemName);
                //    MediaItem sampleMedia = new MediaItem(Photo);
                //    Sitecore.Data.Fields.ImageField imageField = List.Fields["Photo"];

                //    List.Editing.BeginEdit();
                //    imageField.Clear();
                //    imageField.SetAttribute("mediaid", sampleMedia.ID.ToString());
                //    imageField.SetAttribute("mediapath", sampleMedia.MediaPath.ToString());
                //    imageField.SetAttribute("src", Sitecore.Resources.Media.MediaManager.GetMediaUrl(sampleMedia));

                //    if (!String.IsNullOrEmpty(sampleMedia.Alt))
                //    {
                //        imageField.Alt = sampleMedia.Alt;
                //    }
                //    else
                //    {
                //        imageField.Alt = sampleMedia.DisplayName;
                //    }
                //    List.Editing.EndEdit();
                //}
            }
            catch (Exception ex)
            {
                throw new Exception("Error Message : " + ex.Message);
                List.Editing.CancelEdit();
            }


        }


        [HttpPost]
        public ActionResult GetAllStationInformationNew()
        {
            try
            {
                /*stationInformation_AllType*/
                List<Models.StationInformationDTO> modelFull = GetAllStationInformation();
                return Json(new { response = modelFull }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
        public List<Models.StationInformationDTO> GetAllStationInformation()
        {
            List<Models.StationInformationDTO> allType = new List<Models.StationInformationDTO>();
            Database DB = Sitecore.Context.Database;
            Sitecore.Data.Items.Item stationInfo_Item = DB.GetItem("/sitecore/content/Home/iMap/Configuration/Station Information/");
            foreach (Sitecore.Data.Items.Item child in stationInfo_Item.Children)
            {

                Sitecore.Data.Fields.Field temp = child.Fields[Templates.StationInformation.StationName];

                StationInformationDTO stationInfoAll = new StationInformationDTO();
                stationInfoAll.stationName = child.Fields[Templates.StationInformation.StationName].ToString();
                stationInfoAll.CRSCode = child.Fields[Templates.StationInformation.CRSCode].ToString();
                allType.Add(stationInfoAll);
            }

            return allType;
        }

    }
}