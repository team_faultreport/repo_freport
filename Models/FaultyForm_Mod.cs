﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FaultyReportingForm.Models
{

    public class StationInformationDTO
    {
        public string stationName { get; set; }
        public string CRSCode { get; set; }

    }
    public class FaultyForm_Mod
    {

        [Required]
        [StringLength(19, MinimumLength = 3)]
        public string firstName { get; set; }

        [Required]
        [StringLength(29, MinimumLength = 3)]
        public string lastName { get; set; }

        [Required]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6", ErrorMessage = "Please enter a proper email address.")]
        public string email { get; set; }

        [Required]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Please enter a proper telephone number.")]
        public string phone { get; set; }

        [Required]
        public string faultType { get; set; }

        //[StringLength(19, MinimumLength = 4)]
        public string departureStation { get; set; }

        public string departureTime { get; set; }


        //[StringLength(19, MinimumLength = 4)]
        public string destinationStation { get; set; }

        [StringLength(19, MinimumLength = 4)]
        public string faultlocation { get; set; }

        

        [DataType(DataType.Date)]
        public DateTime faultTime { get; set; }

        [Required]
        public string faultDescription { get; set; }

        public string faultLocationDropdown { get; set; }
        public string StnDepartureStation { get; set; }
        public string stnFaultDescription { get; set; }

        public string Other { get; set; }





        //public string file { get; set; }
        //public HttpPostedFileBase file { get; set; }


    }
   
}