﻿$(function () {
    var firstName = false;
    var lastName = false;
    var email = false;
    var phoneNo = false;
    var faultLocation = true;
    var faultDescription = false;
    var txtStnfaultDescription = false;

    var url = $(location).attr('href');
    $('#spn_url').html('<strong>' + url + '</strong>');
    $("#submit").click(function () {
    });


    $("#Station").change(function (e) {
        if (this.value == "Please select one") {
            $("#Form2").hide();
            $("#Form3").hide();
        }
        else if (this.value === "On a SWR train") {
            $("#Form3").hide();
            $("#Form2").show();
            $(".errors_FaultType").remove();
            txtStnfaultDescription = true;


        }
        else {
            (this.value === "At a rail station")
            {
                $("#Form2").hide();
                $("#Form3").show();
                $(".errors_FaultType").remove();
                faultDescription = true;
                faultLocation = true;
            }

        }
    });
    $("#FaultArea").change(function (e) {
        if (this.value == "Other") {
            $("#Other").show();
        }
        else {
            $("#Other").hide();
        }
    });


    //firstName
    $("#txtFirstName").focusout(function () {
        $("#FName").find("span").remove();
        var value = $(this).val();
        //$(".errors_first").remove();
        if (value.length < 3 && value.length > 0) {
            $("#txtFirstName").after('<span class="errors_first validation-error-message">Length of first name should be at least 3 charaters.</span>');
            //$(".errors_first").remove();
            return false;
        }
        else if (value.length > 20) {
            $("#txtFirstName").after('<span class="errors_firstName validation-error-message">Length of first name cannot exceed 20 characters.</span>');
            return false;
        }

        else if (value.length == 0) {
            $("#txtFirstName").after('<span class="errors_firstName validation-error-message">This field is required.</span>');
            return false
        }
        else {
            firstName = true;
        }
    });
    //lastname
    $("#txtLastName").focusout(function () {
        $("#LName").find("span").remove();
        var value = $(this).val();
        //$(".errorsSN").remove();
        if (value.length < 3 && value.length > 0) {
            $("#txtLastName").after('<span class="errorsSN validation-error-message">Length of last name should be at least 3 charaters.</span>');
            return false;
        }
        else if (value.length > 20) {
            $("#txtLastName").after('<span class="errorsSN validation-error-message">Length of last name cannot exceed 20 characters.</span>');
            return false;
        }
        else if (value.length == 0) {
            $("#txtLastName").after('<span class="errorsSN validation-error-message">This field is required.</span>');
            return false;
        }
        else {
            lastName = true;
        }
    });
    //email
    $("#txtemail").focusout(function () {
        $("#Email").find("span").remove();
        var inputVal = $("#txtemail").val();
        //var characterReg = new RegExp("[a-zA-Z0-9\\.]+@[a-zA-Z0-9\\-\\_\\.]+\\.[a-zA-Z0-9]{3}");
        var characterReg = new RegExp(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/);
        if (inputVal.length == 0) {
            $("#txtemail").after('<span class="Email validation-error-message">This field is required.</span>');
            return false;
        }
        else if (!characterReg.test(inputVal)) {
            $("#txtemail").after('<span class="Email validation-error-message">Please enter a valid email address.</span>');
            return false;
        }
        else {
            email = true;

        }
    });

    //phone
    $("#txtphone").focusout(function () {
        $("#Phone").find("span").remove();
        var inputVal = $("#txtphone").val();
        //var characterReg = new RegExp("^([0|\+[0-9]{1,5})?([7-9][0-9]{9})$");
        var characterReg = new RegExp(/^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$/);
        if (inputVal.length == 0) {
            phoneNo = true;
        }
        else if (!characterReg.test(inputVal)) {
            $("#txtphone").after('<span class="Phone validation-error-message">Please enter a proper telephone number.</span>');
            return false;
        }
        //else {
        //    phoneNo = true;

        //}
    });

    //txtfaultlocation
    $("#txtfaultlocation").focusout(function () {
        $("#FaultLocation").find("span").remove();
        var value = $(this).val();
        $(".errors").remove();
        if (value.length < 3 && value.length > 0) {
            $("#txtfaultlocation").after('<span class="FaultLocation validation-error-message">Length of fault location is too short.</span>');
            return false;
        }
        else if (value.length > 20) {
            $("#txtfaultlocation").after('<span class="FaultLocation validation-error-message">Length of fault location is too long.</span>');
            return false;
        }
        else {
            faultLocation = true;
        }
    });

    //txtfaultDescription
    $("#txtfaultDescription").focusout(function () {
        $("#FaultDescription").find("span").remove();
        var value = $(this).val();
        $(".errors").remove();
        if (value.length == 0) {
            $("#txtfaultDescription").after('<span class="FaultDescription validation-error-message">This field is required.</span>');
            return false;
        }
        else if (value.length > 1500) {
            $("#txtfaultDescription").after('<span class="FaultDescription validation-error-message">Length of fault description is too long.</span>');
            return false;
        }
        else {
            faultDescription = true;
        }
    });

    //txtStnfaultDescription
    $("#txtStnfaultDescription").focusout(function () {
        $("#stnFaultDescription").find("span").remove();
        var value = $(this).val();
        $(".errors").remove();
        if (value.length == 0) {
            $("#txtStnfaultDescription").after('<span class="stnFaultDescription validation-error-message">This field is required.</span>');
            return false;
        }
        else if (value.length > 1500) {
            $("#txtStnfaultDescription").after('<span class="stnFaultDescription validation-error-message">Length of fault description is too long.</span>');
            return false;
        }
        else {
            txtStnfaultDescription = true;
        }
    });

    $("#txtdestinationStation").focusout(function () {
        //&& this.value > 0
        debugger
        $("#destinationCheck").find("span").remove();
        if (this.value == $("#txtdepartureStation").val()) {
            if (this.value.length > 0) {
                $("#txtdestinationStation").after('<span class="txtdestinationStation validation-error-message">Departure and destination stations cannot be same.</span>');
                return false;
            }
        }

    });

    //$("#txtdepartureStation").focusout(function () {
    //    debugger
    //    $("#departureCheck").find("span").remove();
    //    if (this.value == $("#txtdestinationStation").val() && this.value > 0) {
    //        $("#txtdepartureStation").after('<span class="txtdepartureStation">Departure and destination stations cannot be same.</span>');
    //        return false;
    //    }


    //});


    ///
    $(window).on('beforeunload', function () {
        $("input[type=submit], input[type=button]").prop("disabled", "disabled");
    });     

    $("#submit").click(function () { 
        //$(".errors_FaultType").remove();

        //$("input[type=submit]").attr("disabled", "disabled");
        $("#general").find("span").remove();
        //if ($("#Station").val() == "Please select one") {
        //    $("#Station").after('<span class="errors_FaultType">Please select the Fault Type.</span>');
        //    return false;
        //}
        debugger
        if (firstName == false || lastName == false || email == false || faultLocation == false || faultDescription == false || txtStnfaultDescription == false) {
            $("#file").after('<span class="general validation-error-message">Please fill all required fields correctly.</span>');
            return false;
            //alert("Please fill all required fields correctly")
            //return false;
        }
        else {
            //$('#submit').prop('disabled', true);
            $('#lblloader').show();
        }

    });

    $('#file').bind('change', function () {

        //5242880 for 5 mb
        $(".errors_size").remove();
        if (this.files[0].size > 5242880) {
            $("#file").after('<span class="errors_size validation-error-message">Please upload image with size less than 5 MB in size.</span>');
            return false;
        }
        else {
            $(".errors_size").remove();
        }

    });


});

function recaptchaCallback() {
    $('#submit').removeAttr('disabled');
};



$(document).ready(function () {

    $('#lblloader').hide();
    $("#Form3").hide();
    $("#Form2").hide();
    //$("#faultTime").val(new Date());
    //$('.date').fdatepicker({
    //    dateFormat: "dd/mm/yyyy hh:mm"
    //});
    var d = new Date();
    var currentmonth = d.getMonth() + 1;
    var time = d.getHours() + ":" + d.getMinutes();
    var dates = d.getDate() + "/" + currentmonth + "/" + d.getFullYear();
    $('.date').val(dates + ' ' + time);

    var finaldate = new Date();
    //finaldate.setDate(d.getDate() - 90);
    finaldate.setMonth(-2);

    //$('.from-date-picker').datetimepicker({
    //    controlType: 'select',
    //    oneLine: true,
    //    dateFormat: 'dd/mm/yyy',
    //    timeFormat: 'hh:mm tt',

    //    minDate: finaldate,
    //    maxDate: 'dt'

    //});
    $('.input-group-addon').click(function () {
        $('.dropdown-menu').show();
    })
    $('#time').timepicker();
    $("#Other").hide();
});

